# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd

app = dash.Dash(__name__)
server = app.server

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options

weather = pd.read_csv("weather.csv.gz")
df = weather.groupby('month').mean()[weather.columns[4:]]
df['month'] = pd.Series(['0','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'])
fig1 = px.line(df, x='month', y=['wind_speed_EWR', 'wind_speed_JFK','wind_speed_LGA'])
fig1.update_layout(title='Average wind speed in each month of Airports EWR, JFK, and LGA',   
                xaxis_title='Month',  
                yaxis_title='Wind speed',
                legend_title_text = 'Different Airports')


flights = pd.read_csv("flights.csv.gz")
flights_EWR = flights[flights['origin'] == 'EWR']
flights_JFK = flights[flights['origin'] == 'JFK']
flights_LGA = flights[flights['origin'] == 'LGA']
flights_total = pd.concat([flights_EWR, flights_JFK, flights_LGA],axis=0,join='outer')
d = { 'Wind_Speed': pd.concat([df['wind_speed_EWR'], df['wind_speed_JFK'], df['wind_speed_LGA']],axis=0,join='outer').values,
    'Depart_Number' : flights_total.groupby(['origin', 'month']).count()['year'].values,
     'Month': pd.Series(['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec',
                        'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec',
                        'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec']),
     'Airports': pd.Series(['EWR','EWR','EWR','EWR','EWR','EWR','EWR','EWR','EWR','EWR','EWR','EWR',
                            'JFK','JFK','JFK','JFK','JFK','JFK','JFK','JFK','JFK','JFK','JFK','JFK',
                            'LGA','LGA','LGA','LGA','LGA','LGA','LGA','LGA','LGA','LGA','LGA','LGA'])
}
data = pd.DataFrame(d)
fig2 = px.scatter(data, x='Wind_Speed', y='Depart_Number', color='Month')
fig2.update_layout(title='Average wind speed in each month VS Number of Departure in Airports EWR, JFK, and LGA',   
                xaxis_title='Wind Speed',  
                yaxis_title='Number of Departure',
                legend_title_text = 'Different Month',
                margin={"r":0,"t":50,"l":0,"b":50})


app.layout = html.Div(children=[
    html.H1(children='Two Figures about the weather data and Flight data'),

    html.Div(children='''
        Figure 1: Average wind speed in each month of Airports EWR, JFK, and LGA. 
        In the part, we want to test the wind speeds in different month and airports. 
        From the figure 1, for three airports, the average wind speed of JFK greater than LGA, and LGA is greater than EWR.
        About the month, the wind speeds from Feb to Apr are greater than ones from Jun to Sept. 
        We may conclude that for different months and different airports, the wind speeds are different. 
    '''),

    dcc.Graph(
        id='example-graph',
        figure=fig1
    ),

    html.Div(children='''
        Figure 2: This is the figure showing Average wind speed in each month VS Number of Departure in Airports EWR, JFK, and LGA. 
        Since from the figure 1, for different months, the wind speeds are different. 
        Thus, in this question, want to test whether the wind speed will affect the number of departure or not. 
        In the Figure 2, there is a decreasing trend when wind speed increases. 
        So we may conclude that wind speed will affect the number of departure, but further analysis is needed since maybe some other factors result. 
    '''),

    dcc.Graph(
        id='example-graph2',
        figure=fig2
    )
])

if __name__ == '__main__':
    app.run_server(debug=True, port=8080)
